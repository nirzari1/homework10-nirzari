#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:	
	Customer(std::string name);
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set
	bool checkExists(Item item);
	//get and set functions
	void setName(std::string name);
	void addCountToCertainItem(Item item);
	void printAllItems();
	bool isItemInList(int serialNum);
	void removeCountToCertainItem(Item item);
	void clearAllItems();

private:
	std::string _name;
	std::set<Item> _items;


};
