#pragma once
#include<iostream>
#include<string>
#include<algorithm>


class Item
{
public:
	Item(std::string name, std::string serialNum, double unitPrice);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator<(const Item& other) const; //compares the _serialNumber of those items.
	bool operator>(const Item& other) const; //compares the _serialNumber of those items.
	bool operator==(const Item& other) const; //compares the _serialNumber of those items.

	
	//get and set functions
	std::string getName() const;
	std::string getSerialNumber() const;
	int getCount() const;
	double getUnitPrice() const;
	void printContents() const;
	void addCount();
	int _count; //default is 1, can never be less than 1!
private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	double _unitPrice; //always bigger than 0!

};