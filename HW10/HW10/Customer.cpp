#include "Customer.h"


Customer::Customer(std::string name)
{
	this->_name = name; //c'tor
}
Customer::~Customer()
{
	//dtor
}
void Customer::removeItem(Item item)
{
	if (this->_items.find(item) != this->_items.end())
	{
		this->_items.erase(item); // fnc to remove item
	}
}
double Customer::totalSum() const
{
	double sum = 0;
	for (std::set<Item>::iterator it1 = this->_items.begin(); it1 != this->_items.end(); it1++)
	{
		sum += it1->totalPrice(); // fnc to calculate total sum by using iterator to run all over the set
	}
	return sum;
}
void Customer::setName(std::string name)
{
	this->_name = name; // setter for name
}
void Customer::addItem(Item item)
{
	this->_items.insert(item); // setter for item
}
void Customer::addCountToCertainItem(Item item)
{
	std::set<Item>::iterator it = _items.find(item);
	int howMany = it->getCount();
	howMany++;
	_items.erase(item);
	item._count = howMany;
	_items.insert(item); // adds count to a certain item in the bucket list
}
void Customer::clearAllItems()
{
	if (_items.size() > 0)
	{
		_items.clear(); // removes the entire set values
	}
}
bool Customer::checkExists(Item item)
{
	std::set<Item>::iterator it = _items.find(item);
	if (it == _items.end())
	{
		return false; // doesnt exists
	}
	return true; // exists
}
void Customer::printAllItems()
{
	std::set<Item>::iterator it = _items.begin();
	for (int i = 0; it != _items.end(); i++)
	{
		it->printContents();
		it++; // prints all contents of a customer
	}
}
bool Customer::isItemInList(int serialNum)
{
	std::set<Item>::iterator it = _items.begin();
	int counter = 0;
	for (int i = 0; it != _items.end(); i++)
	{
		if (std::stoi(it->getSerialNumber()) == serialNum)
		{
			return true; // checks if item is in a list by moving it from std::string to int using stoi
		}
		it++;
	}
	return false;
}
void Customer::removeCountToCertainItem(Item item)
{
	std::set<Item>::iterator it = _items.find(item);
	int howMany = it->getCount();
	howMany--;
	_items.erase(item);
	item._count = howMany;
	if (howMany > 0)
	{
		_items.insert(item); // checks if count is 0, if it is, dont insert it into _items
	}
}