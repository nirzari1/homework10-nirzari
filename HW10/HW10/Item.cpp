#include "Item.h"

Item::Item(std::string name, std::string serialNum, double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serialNum;
	this->_unitPrice = unitPrice;
	this->_count = 1; // ctor (default _count value)
}
Item::~Item()
{
	//dtor
}
std::string Item::getSerialNumber() const
{
	return this->_serialNumber; //getter for serial num
}
std::string Item::getName() const
{
	return this->_name; //getter for name
}
int Item::getCount() const
{
	return this->_count; //getter for count
}
void Item::printContents() const
{
	std::cout << "Item name: " << this->_name << " | ";
	std::cout << "Item price: " << this->_unitPrice << " | ";
	std::cout << "How many you bought: " << this->_count << " | ";
	std::cout << "Serial number: " << this->_serialNumber << std::endl; // prints contents of an item
}
void Item::addCount()
{
	_count++; // adds 1 to count
}

double Item::getUnitPrice() const
{
	return this->_unitPrice; // gets price of item
}
double Item::totalPrice() const
{
	return this->_count * this->_unitPrice; // gets total price of item (count)
}
bool Item::operator<(const Item& other) const
{
	return (this->_serialNumber < other._serialNumber); // operator <
}
bool Item::operator>(const Item& other) const
{
	return (this->_serialNumber > other._serialNumber); // operator >
}
bool Item::operator==(const Item& other) const
{
	return (this->_serialNumber == other._serialNumber); // opeartor ==
}