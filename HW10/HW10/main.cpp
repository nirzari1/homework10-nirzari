#include"Customer.h"
#include<map>

void printFunc()
{
	std::cout << "Welcome to MagshiMart!" << std::endl;
	std::cout << "To sign as customer and buy items, press 1" << std::endl;
	std::cout << "To update existing customer items, press 2" << std::endl;
	std::cout << "To print the customer who pays the most, press 3" << std::endl;
	std::cout << "To exit, press 4" << std::endl; // funtcion to print menu
}
void printAllItems(Item* list, int size)
{
	std::cout << "(Press 0 To Exit)" << std::endl;
	for (int i = 0; i < size; i++)
	{
		std::cout << "Item number " << i+1 << " : ";
		list[i].printContents();
	}
	std::cout << "What item would you like to buy?" << std::endl; // function to print all of the menu
}

int main()
{
	int userChoice = 0;
	double mostPaid = 0;
	double howMuchPaid = 0;
	Customer addCustomer("");
	Customer* addCustomerPtr;
	bool dontHaveName = false;
	std::map<std::string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00007", 31.65),
		Item("chicken","00008",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};
	std::string name = "";
	std::map<std::string, Customer>::iterator it;
	std::set<Item> it1;
	
	while (userChoice != 4)
	{
		howMuchPaid = 0;
		mostPaid = 0;
		dontHaveName = false;
		system("cls");
		printFunc();
		std::cin >> userChoice;
		while (userChoice > 4 || userChoice < 1)
		{
			std::cout << "Enter again num 1-4!";
			std::cin >> userChoice; // while user dont enter num 1-4, keeps asking for input
		}
		switch (userChoice)
		{
			case 1: // add new customer
				std::cout << "Enter your name: ";
				std::cin >> name;
				it = abcCustomers.begin();
				for (unsigned int i = 0; i < abcCustomers.size(); i++)
				{
					if (it->first == name)
					{
						dontHaveName = true;
					}
					it++; // checks if the name doesnt exist in the abcCustomers map
				}
				if(!dontHaveName) // if name dont exist
				{
					addCustomer.clearAllItems();
					addCustomer.setName(name); // clears all items and sets a name, then prints a list of all of the items in shop
					printAllItems(itemList, 10);
					while (userChoice != 0)
					{
						std::cin >> userChoice;
						while (userChoice > 10 || userChoice < 0)
						{
							std::cout << "Try again: ";
							std::cin >> userChoice; // while user choice isnt 0-10
						}
						if (userChoice != 0)
						{
							if (addCustomer.checkExists(itemList[userChoice - 1])) // if item exists, need to add count
							{
								addCustomer.addCountToCertainItem(itemList[userChoice - 1]); // adds 1 count to item if exist
							}
							else
							{
								addCustomer.addItem(itemList[userChoice - 1]); // if item doesnt exist
							}
						}
						std::cout << "Enter again: ";
					}
					abcCustomers.insert({ name, addCustomer }); // at the end adds the customer to abcCustomers map
				}
				else
				{
					std::cout << "Name already exists!" << std::endl;
					system("pause"); // if the name already exists, lets user know
				}
				break;
			case 2: // update customer info
				std::cout << "Enter your name: ";
				std::cin >> name;
				it = abcCustomers.begin();
				for (unsigned int i = 0; i < abcCustomers.size(); i++)
				{
					if (it->first == name)
					{
						dontHaveName = true;
					}
					it++; // checks if name exists
				}
				if (dontHaveName) // if name exists
				{
					std::cout << "Enter options 1-3: " << std::endl;
					std::cout << "1: Add items" << std::endl;
					std::cout << "2: Remove items" << std::endl;
					std::cout << "3: Back to menu" << std::endl;
					std::cout << "Make your choice: ";
					std::cin >> userChoice;  // asks user for his choice
					while (userChoice > 3 || userChoice < 1)
					{
						std::cout << "Wrong input, try again!" << std::endl;
						std::cin >> userChoice; // while user didnt enter 1-3
					}
					switch (userChoice)
					{
						case 1:
							addCustomerPtr = &abcCustomers.at(name);
							printAllItems(itemList, 10); // prints list of items
							while (userChoice != 0) // while user didnt chose to exit (0)
							{
								std::cin >> userChoice;
								while (userChoice > 10 || userChoice < 0)
								{
									std::cout << "Try again: ";
									std::cin >> userChoice; // gets input 0-10
								}
								if (userChoice != 0)
								{
									if (addCustomerPtr->checkExists(itemList[userChoice - 1])) // if item exists, need to add count
									{
										addCustomerPtr->addCountToCertainItem(itemList[userChoice - 1]);
									}
									else
									{
										addCustomerPtr->addItem(itemList[userChoice - 1]); // if item doesnt exist
									}
								}
								std::cout << "Enter again: ";
							}
							break;
						case 2: // user wants to remove item
							addCustomerPtr = &abcCustomers.at(name); // gets customer through fnc at()
							while (userChoice != 0)
							{
								addCustomerPtr->printAllItems(); // prints all items
								std::cout << "What would you like to remove? (Enter serial number please): " << std::endl; // asks for serial number
								std::cin >> userChoice;
								while (userChoice < 0 || !addCustomerPtr->isItemInList(userChoice)) // if the item exists
								{
									std::cout << "Try again: " << std::endl;
									std::cin >> userChoice; // gets user choice until picks item that exists in his bucket list
								}
								if (userChoice != 0)
								{
									addCustomerPtr->removeCountToCertainItem(itemList[userChoice - 1]); // removes item if he chose to remove it
								}
							}
							
							system("pause");
							break;
						case 3:
							break;
					}
				}
				else
				{
					std::cout << "Customer dont exist!" << std::endl; // lets user know the customer he entered doesnt exist
					system("pause");
				}
				break;
			case 3:
				for (it = abcCustomers.begin(); it != abcCustomers.end(); it++)
				{
					howMuchPaid = it->second.totalSum(); // gets total sum
					if (howMuchPaid > mostPaid)
					{
						mostPaid = howMuchPaid;
						name = it->first; // checks who paid the most
					}
				}
				std::cout << "Most paying customer: " << name << ", Amount: " << mostPaid << std::endl; // prints who paid the most and how much
				system("pause");
				break;
			case 4:
				break;
		}
	}
	std::cout << "Bye!" << std::endl;


	return 0;
}